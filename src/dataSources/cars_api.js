const { RESTDataSource } = require('apollo-datasource-rest');

const serverConfig = require('../server');

class AuthAPI extends RESTDataSource{

    constructor(){
        super();
        this.baseURL = serverConfig.car_manage   
    }
    async allCars(){
        return await this.get(`/api-v1/all-cars/`)
    }
    async search(){
        return await this.put(`/api-v1/cars/search/`)
    }
    async threeCars(){
        return await this.get(`/api-v1/three-cars/`)
    }
    async categoria(categoria){
        return await this.get(`/api-v1/${categoria}/`)
    }
    async singleCar(categoria, car){
        return await this.get(`/api-v1/${categoria}/${car}`)
    }

}

module.exports = AuthAPI