const { gql } = require('apollo-server');

const authTypeDefs = gql `
    type Car{
        id: Int!
        nombre: String
        get_absolute_url: String
        descripcion: String!
        marca: Int
        categoria: Int
        disponibilidad: Boolean
        precio: Float
        get_image: String
        get_thumbnail: String
    }

    type Reserva{
        id: Int
        fecha_inicio: String
        fecha_final: String
        nombre-de-usuario: String!
        car: Int!
    }


`;

module.exports = authTypeDefs;