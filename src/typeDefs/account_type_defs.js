const { gql } = require('apollo-server');

const accountTypeDefs = gql `
    type Reservation {
        username: String!
        balance: Int!
        lastChange: String!
    }
    
    extend type Query {
        reservationByUserName(username: String!): Reservation
    }
`;

module.exports = accountTypeDefs;